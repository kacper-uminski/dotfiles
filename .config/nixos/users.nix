{pkgs, lib, config, ...}:
{
  users.users.kacper = {
    description = "Kacper Uminski";
    extraGroups = [
      "wheel"
      "networkmanager"
    ] ++ (lib.lists.optionals config.laptop.enable [
      "video"
    ]);
    isNormalUser = true;
    shell = pkgs.zsh;
  };
}
