{pkgs, nixpkgs-stable, config, system, ...}:
{
  environment = {
    pathsToLink = [
      "/share/bash-completion"
      "/share/zsh"
    ];

    sessionVariables = {
      NIXOS_OZONE_WL = "1";
    };

    systemPackages = import ./packages/main.nix {
      inherit pkgs;
      inherit nixpkgs-stable;
      inherit config;
      inherit system;
    };
  };
}
