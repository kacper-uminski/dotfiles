{lib, config, ...}:
{
    virtualisation = lib.mkIf config.desktop.enable {
      libvirtd.enable = false;
      spiceUSBRedirection.enable = false; 
    };
}
