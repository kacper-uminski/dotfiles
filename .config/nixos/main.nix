{config, home-manager, lib, nix-colors, nixvim, ...}:
{
  options = {
    desktop.enable = lib.mkEnableOption "Enable desktop config.";
    laptop.enable = lib.mkEnableOption "Enable laptop config.";
    themes = {
      base16.enable = lib.mkDefault false;
      challenger-deep.enable = lib.mkDefault false;
    };
  };

  imports = [
    ./bluetooth.nix
    ./boot.nix
    ./environment.nix
    ./fonts.nix
    ./locale.nix
    ./mount.nix
    ./networking.nix
    ./programs.nix
    ./services.nix
    ./sound.nix
    ./users.nix
    ./virtualisation.nix
    home-manager.nixosModules.home-manager {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        users.kacper = import ./home/main.nix;
        extraSpecialArgs = {
          inherit nix-colors;
          inherit nixvim;
          desktop = config.desktop.enable;
          laptop = config.laptop.enable;
        };
      };
    }
  ];

  config = {
    nix.settings.experimental-features = [ "flakes" "nix-command" ];
    nixpkgs.config.allowUnfree = true; 
    system.stateVersion = if config.desktop.enable then "23.11" else "23.05";
  };
}
