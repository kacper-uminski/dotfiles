{pkgs, nixpkgs-stable, system}:
let
  stable = import nixpkgs-stable {
    inherit system;
    config.allowUnfree = true;
  };
in with pkgs; [
  clinfo
  darktable
  flameshot
  prismlauncher
  puddletag
  pulsemixer
  qbittorrent
  stable.retroarchFull
  screen
  shntool
  skypeforlinux
  slack
  tdesktop
  xmrig
  xorg.xset
  ryujinx
]
