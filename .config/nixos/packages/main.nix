{pkgs, nixpkgs-stable, system, config, ...}:
with pkgs; [
  (import ./python.nix pkgs)
  (import ./haskell.nix pkgs)
  aspell
  aspellDicts.sv
  bat
  bear # Allows LSP to find #include non-std files and headers.
  cargo-binutils
  cbqn
  # cifs-utils
  clang-tools_17
  discord
  elixir
  elixir-ls
  erlang
  erlang-ls
  evince
  exercism
  ffmpeg
  file
  firefox
  ghdl
  # gnuapl
  # gnuplot
  # gradience
  gradience
  gtkwave
  home-manager
  jetbrains.idea-community
  julia-bin
  msr # Used by xmrig.
  nil
  octaveFull
  rebar3 # Erlang build system.
  rustup
  sshfs
  swww # Wallpaper daemon for wayland.
  typst
  typstfmt
  typst-lsp
  uiua
  unzip
  usbutils
  # valgrind # Memory profiler for C/C++
  vhdl-ls
  vifm
  wget
  wiki-tui
  wlr-randr
  zip
]
++ (lib.lists.optionals config.desktop.enable (import ./desktop.nix {
  inherit pkgs;
  inherit nixpkgs-stable;
  inherit system;
}))
++ (lib.lists.optionals config.laptop.enable (import ./laptop.nix {inherit pkgs;}))
++ [(import ./sacd_extract.nix (with pkgs; {
  inherit lib;
  inherit cmake;
  inherit libxml2;
  inherit stdenv;
  fetchurl = builtins.fetchurl;
}))]
