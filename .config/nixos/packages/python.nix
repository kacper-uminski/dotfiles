pkgs: pkgs.python3.withPackages(python-packages: with python-packages; [
  flake8
  matplotlib
  mypy
  pylsp-mypy
  python-lsp-server
  sympy
])
