{lib, config, ...}:
{
  boot = {
    initrd.kernelModules = lib.mkIf config.desktop.enable [ "amdgpu" ];
    kernelModules = lib.mkIf config.desktop.enable [ "msr" "i2c-dev" "i2c-piix4" ];
    loader = {
      systemd-boot.enable = true;
      efi = {
        efiSysMountPoint = "/boot";
        canTouchEfiVariables = true;
      };
    };
  };
}
