{
  description = "NixOS configuration";

  inputs = {
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-colors.url = "github:misterio77/nix-colors";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-23.11";
    nixvim = {
        url = "github:nix-community/nixvim";
        inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { home-manager, nix-colors, nixpkgs, nixpkgs-stable, nixvim,  ... }: {
    nixosConfigurations = let
      mkConfig = {desktop, laptop, hardware}: nixpkgs.lib.nixosSystem rec {
        system = "x86_64-linux";
        specialArgs = {
          inherit home-manager;
          inherit nix-colors;
          inherit nixpkgs;
          inherit nixpkgs-stable;
          inherit nixvim;
          inherit system;
        };
        modules = [
          hardware
          ./main.nix {
            desktop.enable = desktop;
            laptop.enable = laptop;
          }
        ];
      };
    in {
      desktop = mkConfig {
        desktop = true;
        laptop = false;
        hardware = ./hardware-configuration-desktop.nix;
      };
      laptop = mkConfig {
        desktop = false;
        laptop = true;
        hardware = ./hardware-configuration-desktop.nix;
      };
    };
  };
}
