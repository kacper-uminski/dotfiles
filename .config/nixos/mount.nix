{lib, config, ...}:
{
  fileSystems."/home/kacper/Media" = lib.mkIf config.desktop.enable {
    device = "192.168.50.200:/mnt/Tank/Media";
    fsType = "nfs";
  };
}
