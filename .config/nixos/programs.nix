{lib, config, ...}:
{
  programs = {
    dconf.enable = lib.mkIf config.desktop.enable true;

    hyprland = {
      enable = true;
      xwayland.enable = true;
    };

    light.enable = lib.mkIf config.laptop.enable true;

    steam.enable = lib.mkIf config.desktop.enable true;

    zsh.enable = true;
  };
}
