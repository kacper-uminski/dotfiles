{lib, config, ...}:
{
  networking = lib.mkIf config.desktop.enable  {
    hostName = "nixos";
    firewall.enable = false;
  } // lib.mkIf config.desktop.enable {
    defaultGateway = "192.168.50.1";
    nameservers = ["192.168.50.200"];
    interfaces.enp14s0.ipv4.addresses = [{
      address = "192.168.50.250";
      prefixLength = 24;
    }];
  } // lib.mkIf config.laptop.enable {
    networkmanager.enable = true;
  };
}
