{pkgs, lib, config, ...}:
{
  services = {
    gnome.gnome-keyring.enable = true;

    hardware.openrgb = lib.mkIf config.desktop.enable {
      enable = true;
      package = pkgs.openrgb-with-all-plugins;
    };

    udev = lib.mkIf config.desktop.enable {
      packages = with pkgs; [
        game-devices-udev-rules
      ];
    };

    openssh.enable = true;
  };
}
