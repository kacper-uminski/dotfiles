{pkgs, ...}:
{

  nixpkgs.config.packageOverrides = pkgs: {
    nerdfonts = pkgs.nerdfonts.override{fonts = ["IBMPlexMono"];};
  };

  # Set system fonts.
  fonts.packages = with pkgs; [
    #barlow
    # fira
    # fira-code
    font-awesome
    #garamond-libre
    ibm-plex
    nerdfonts
  ];
}
