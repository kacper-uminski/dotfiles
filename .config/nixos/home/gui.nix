{pkgs, lib, config, ...}:
{
  gtk = {
    enable = true;
    cursorTheme.name = "Breeze";
    theme = {
      package = pkgs.adw-gtk3;
      name = "adw-gtk3-dark";
    };
  };

  programs = {
    alacritty = {
      enable = true;
      settings = {
        font = {
          normal.family = "IBM Plex Mono";
          size = 12;
        };
        window = {
          opacity = lib.mkIf config.laptop.enable 0.8;
          padding = lib.mkIf config.desktop.enable {
            x = 20;
            y = 20;
          };
        };
      };
    };

    chromium = {
      enable = false;
      commandLineArgs = [
        "--ozone-platform-hint=auto"
        "--enable-features=TouchpadOverscrollHistoryNavigation"
      ];
      package = pkgs.chromium;
    };


    feh.enable = true;

    mpv = {
      enable = true;
      config = {
        hwdec = "auto-safe";
        vo = "gpu";
        profile = "gpu-hq";
      };
    };


    rofi = {
      enable = true;
      package = pkgs.rofi-wayland;
      terminal = "${pkgs.alacritty}/bin/alacritty";
    };



    zathura = {
      enable = true;
      options = {
        default-bg = "#000000";
        default-fg = "#FFFFFF";
        guioptions = "none";
        recolor = true;
      };
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk3";
  };

}
