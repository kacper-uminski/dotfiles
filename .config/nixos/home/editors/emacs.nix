{pkgs, ...}:
{
  
  programs.emacs = {
    enable = true;
    package = pkgs.emacs29-pgtk;
  };

  services.emacs = {
    # Enable emacs daemon.
    enable = true;
    defaultEditor = true;
  };

  home.shellAliases.emc = "emacsclient -nw";
}
