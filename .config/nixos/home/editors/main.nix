{pkgs, ...}:
{
  imports = [
    ./emacs.nix
    ./helix.nix
    ./nixvim.nix
  ];
}
