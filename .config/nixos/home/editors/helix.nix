{pkgs, ...}:
{
  programs.helix = {
    enable = true;
    settings = {
      editor = {
        lsp.display-messages = true;
        cursor-shape.insert = "bar";
      };
      keys.normal = {
        C-g = [":new" ":insert-output ${pkgs.lazygit}/bin/lazygit" ":buffer-close!" ":redraw"];
      };
    };
  };
}
