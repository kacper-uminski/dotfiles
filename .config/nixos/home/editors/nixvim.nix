{lib, ...}:
{
  programs.nixvim = {
    enable = true;
    clipboard.register = "unnamedplus";
    globals = {
      mapleader = " ";
      maplocalleader = " ";
    };
	
    opts = {
      number = true;
      signcolumn = "yes";
      tabstop = 4;
      shiftwidth = 2;
	  expandtab = true;
	  cindent = true;
      updatetime = 300;
      termguicolors = true;
      mouse = "a";
    };
    plugins = {
      cmp = {
        enable = true;
        settings = {
          snippet.expand = "function(args) require('luasnip').lsp_expand(args.body) end";
          mapping = {
            __raw = ''
                    cmp.mapping.preset.insert {
                                              ['<C-n>'] = cmp.mapping.select_next_item(),
                                              ['<C-p>'] = cmp.mapping.select_prev_item(),
                                              ['<C-d>'] = cmp.mapping.scroll_docs(-4),
                                              ['<C-f>'] = cmp.mapping.scroll_docs(4),
                                              ['<C-space>'] = cmp.mapping.complete {},
                                              ['<CR>'] = cmp.mapping.confirm {
                                                       behavior = cmp.ConfirmBehavior.Replace,
                                                       select = true,
                                              },
                                              ['<Tab>'] = cmp.mapping(function(fallback)
                                                        local luasnip = require("luasnip")
                                                        if cmp.visible() then
                                                           cmp.select_next_item()
                                                        elseif luasnip.expand_or_locally_jumpable() then
                                                           luasnip.expand_or_jump()
                                                        else
                                                           fallback()
                                                        end
                                              end, {'i', 's'}),
                                              ['<S-Tab>'] = cmp.mapping(function(fallback)
                                                        local luasnip = require("luasnip")
                                                        if cmp.visible() then
                                                           cmp.select_prev_item()
                                                        elseif luasnip.locally_jumpable(-1) then
                                                           luasnip.jump(-1)
                                                        else
                                                           fallback()
                                                        end 
                                              end, {'i', 's'}),
                    }
                    '';
          };
          sources = [
            { name = "nvim_lsp"; }
            { name = "luasnip"; }
          ];
        };
      };
      cmp-nvim-lsp.enable = true;
      cmp_luasnip.enable = true;
      comment.enable = true;
      diffview.enable = true;
      friendly-snippets.enable = true;
      gitsigns.enable = true;
      lsp = {
        enable = true;
        onAttach = lib.strings.concatLines [
          "local bufmap = function(keys, func) vim.keymap.set('n', keys, func, { buffer = bufnr }) end"
          "bufmap('<leader>r', vim.lsp.buf.rename)"
          "bufmap('<leader>a', vim.lsp.buf.code_action)"
          "bufmap('gd', vim.lsp.buf.definition)"
          "bufmap('gD', vim.lsp.buf.declaration)"
          "bufmap('gI', vim.lsp.buf.implementation)"
          "bufmap('<leader>D', vim.lsp.buf.type_definition)"
          "bufmap('gr', require('telescope.builtin').lsp_references)"
          "bufmap('<leader>s>', require('telescope.builtin').lsp_document_symbols)"
          "bufmap('<leader>s>', require('telescope.builtin').lsp_dynamic_workspace_symbols)"
          "bufmap('K', vim.lsp.buf.hover)"
          "vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_) vim.lsp.buf.format() end, {})"
        ];
        servers = {
          nil_ls.enable = true;
		  typst-lsp.enable = true;
          vhdl-ls.enable = true;
        };
      };
      lualine.enable = true;
      luasnip.enable = true;
      nvim-autopairs.enable = true;
	  rainbow-delimiters.enable = true;
      telescope.enable = true;
      treesitter.enable = true;
	  typst-vim.enable = true;
    };
    viAlias = true;
    vimAlias = true;
  };
}
