{config, lib, ...}:
{

  options = {
    themes.base16.enable = lib.mkEnableOption "Enable base16 theming.";
  };
  config = lib.mkIf config.themes.base16.enable {
    programs.alacritty.settings.colors = with config.colorScheme.palette; {
      cursor = {
        cursor = "#${base06}";
        text = "#${base00}";
      };
      primary = {
        background =  "#${base00}";
        foreground = "#${base07}";
      };
      normal = {
        black = "#${base00}";
        red = "#${base08}";
        green = "#${base0B}";
        yellow = "#${base0A}";
        blue = "#${base0D}";
        magenta = "#${base0E}";
        cyan = "#${base0C}";
        white = "#${base06}";
      };
      bright = {
        black = "#${base03}";
        red = "#${base08}";
        green = "#${base0B}";
        yellow = "#${base09}";
        blue = "#${base0D}";
        magenta = "#${base0E}";
        cyan = "#${base0C}";
        white = "#${base07}";
      };
    };
    
  };
}
