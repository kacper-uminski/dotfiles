{lib, ...}:
{
  imports = [
    ./base16.nix
    ./challenger-deep.nix
    ./gruvbox.nix
  ];
  themes = {
    base16.enable = lib.mkDefault false;
    challenger-deep.enable = lib.mkDefault false;
    gruvbox.enable = lib.mkDefault false;
  };
}
