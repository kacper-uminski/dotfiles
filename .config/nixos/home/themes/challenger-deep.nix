{config, lib, ...}:
{
  options = {
    themes.challenger-deep.enable = lib.mkEnableOption "Enable Challenger Deep theme.";
  };
  config = lib.mkIf config.themes.challenger-deep.enable {
    programs.alacritty.settings.colors = {
      cursor = {
        cursor = "#cbe3e7";
        text = "#1b182c";
      };
      primary = {
        background = "#1b182c";
        foreground = "#cbe3e7";
      };
      normal = {
        black = "#100e23";
        red = "#ff8080";
        green = "#95ffa4";
        yellow = "#ffe9aa";
        blue = "#91ddff";
        magenta = "#c991e1";
        cyan = "#aaffe4";
        white = "#cbe3e7";
      };
      bright = {
        black = "#565575";
        red = "#ff5458";
        green = "#62d196";
        yellow = "#ffb378";
        blue = "#65b2ff";
        magenta = "#906cff";
        cyan = "#63f2f1";
        white = "#a6b3cc";
      };
    };
  };
}
