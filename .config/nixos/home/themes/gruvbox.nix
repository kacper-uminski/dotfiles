{config, lib, nix-colors, ...}:
{
  options = {
    themes.gruvbox.enable = lib.mkEnableOption "Enable Gruvbox theme.";
  };

  config = lib.mkIf config.themes.gruvbox.enable {
    themes.base16.enable = true;
    colorScheme = nix-colors.colorSchemes.gruvbox-dark-hard;
    programs = {
      helix.settings.theme = "gruvbox_dark_hard";
      nixvim.colorschemes.gruvbox.enable = true;
    };
  };
}
