{lib, config, ...}:
{
  programs.waybar = lib.mkIf config.laptop.enable {
    enable = true;
    settings = {
      mainBar = {
        height = 30;
        layer = "top";
        modules-left = [ "hyprland/workspaces" ];
        modules-right = [ "network" "battery" "tray" ];
        output = [ "eDP-1" ];
        position = "top";
        spacing = 4;

        battery = {
          adapter = "ADP1";
          bat = "BAT1";
          format = "{icon}  {capacity}%";
          format-icons = ["" "" "" "" ""];
          format-charging = " {capacity}%";
          states = {
            critical = 15;
            warning = 30;
          };
        };

        "hyprland/workspaces" = {
          format = "{icon}";
          on-scroll-up = "hyprctl dispatch workspace e+1";
          on-scroll-down = "hyprctl dispatch workspace e-1";
        };

        network = {
          format-disconnected = "";
          format-wifi = "";
          interface = "wlp0s20f3";
        };

        tray = {
          icon-size = 21;
          spacing = 10;
        };
      };
    };
    style = ''
                  * {
                      border: none;
                      border-radius: 0;
                      font-family: Plex Mono;
                  }
                  window#waybar {
                      background: #1b182c;
                      color: #cbe3e7;
                  }
                  #network.wifi {
                      margin: 0 10px;
                  }
          '';
  };
}
