{lib, config, ...}:
{
  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      "$term" = "alacritty";
      "$mod" = "SUPER";
      bind = [
        # Go to workspace #n
        "$mod, 1, workspace, 1" 
        "$mod, 2, workspace, 2" 
        "$mod, 3, workspace, 3"
        "$mod, 4, workspace, 4"
        "$mod, 5, workspace, 5"
        "$mod, 6, workspace, 6"
        "$mod, 7, workspace, 7"
        "$mod, 8, workspace, 8"
        "$mod, 9, workspace, 9"
        "$mod, 0, workspace, 10"

        # Move to workspace #n
        "$mod SHIFT, 1, movetoworkspace, 1"
        "$mod SHIFT, 2, movetoworkspace, 2"
        "$mod SHIFT, 3, movetoworkspace, 3"
        "$mod SHIFT, 4, movetoworkspace, 4"
        "$mod SHIFT, 5, movetoworkspace, 5"
        "$mod SHIFT, 6, movetoworkspace, 6"
        "$mod SHIFT, 7, movetoworkspace, 7"
        "$mod SHIFT, 8, movetoworkspace, 8"
        "$mod SHIFT, 9, movetoworkspace, 9"
        "$mod SHIFT, 0, movetoworkspace, 10"

        # Window Cycling and Moving
        "$mod, J, cyclenext"
        "$mod, K, cyclenext, prev"
        "$mod SHIFT, J, swapnext"
        "$mod SHIFT, K, swapnext, prev"

        # Fullscreen
        "$mod, F, fullscreen"

        # Start Programs
        "$mod, R, exec, rofi -show drun -show-icons"
        "$mod, E, exec, emacs"
        "$mod, B, exec, firefox"
        "$mod, T, exec, $term"

        # Kill Current Window
        "$mod, W, killactive"

        # Exit
        "$mod SHIFT, Q, exit"
      ] ++ (lib.lists.optionals config.desktop.enable [
        "$mod ALT, D, exec, hyprctl switchxkblayout tom-wong-cornall/ellipse/wcass/purdea-andrei-modelfkeyboards.com-brand-new-f62/f77-model-f-keyboard-by-model-f-labs 0"
        "$mod ALT, S, exec, hyprctl switchxkblayout tom-wong-cornall/ellipse/wcass/purdea-andrei-modelfkeyboards.com-brand-new-f62/f77-model-f-keyboard-by-model-f-labs 1"
        "$mod ALT, P, exec, hyprctl switchxkblayout tom-wong-cornall/ellipse/wcass/purdea-andrei-modelfkeyboards.com-brand-new-f62/f77-model-f-keyboard-by-model-f-labs 2"
      ]) ++ (lib.lists.optionals config.laptop.enable [
        "$mod ALT, D, exec, hyprctl switchxkblayout microsoft-surface-type-cover-keyboard 0"
        "$mod ALT, S, exec, hyprctl switchxkblayout microsoft-surface-type-cover-keyboard 1"
        "$mod ALT, P, exec, hyprctl switchxkblayout microsoft-surface-type-cover-keyboard 2"
      ]);


      binde = lib.mkIf config.laptop.enable [
        ", XF86MonBrightnessUp, exec, light -A 10"
        ", XF86MonBrightnessDown, exec, light -U 10"
        ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 5%+"
        ", XF86AudioLowerVolume, exec, wpctl set-volume -l 1.0 @DEFAULT_AUDIO_SINK@ 5%-"
        ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
      ];

      bindm = [
        # Moving windows by mouse
        "$mod, mouse:272, movewindow"
        "$mod, mouse:273, resizewindow"
      ];

      exec-once = [
        "swww-daemon &"
        "swww img $HOME/Pictures/Wallpapers/${if config.laptop.enable then "nixos.png" else "black.jpg"} &"
      ] ++ (lib.lists.optionals config.laptop.enable [
        "light -N 1"
        "waybar &"
      ]);

      general = {
        cursor_inactive_timeout = 5;
      };

      gestures = lib.mkIf config.laptop.enable {
        workspace_swipe = true;
        workspace_swipe_invert = false;
      };

      input = {
        kb_layout = "us, se, pl";
        kb_variant = "dvorak, dvorak, dvorak";
        kb_options = "caps:ctrl_modifier";
        touchpad = lib.mkIf config.laptop.enable {
          middle_button_emulation = true;
        };
      };

      dwindle = {
        no_gaps_when_only = 1;
      };

      misc = {
        disable_hyprland_logo = true;
        disable_splash_rendering = true;
        enable_swallow = true;
        swallow_regex = "^(Alacritty)$";
      };

      windowrule = [
        "workspace 1, Alacritty"
      ] ++ (lib.lists.optionals config.desktop.enable [
        "workspace 1, Emacs"
        "workspace 2, Firefox"
        "workspace 3, Skype"
        "workspace 3, TelegramDesktop"
      ]) ++ (lib.lists.optionals config.laptop.enable [
        "workspace 2, Emacs"
        "workspace 3, Firefox"
      ]);

      workspace = [
        "1, on-created-empty:$term, default:true"
      ] ++ (lib.lists.optionals config.desktop.enable [
        "2, on-created-empty:firefox"
        "3, on-created-empty:telegram-desktop & skypeforlinux"
        "4, on-created-empty:steam"
        "5, on-created-empty:darktable"
      ]) ++ (lib.lists.optionals config.laptop.enable [
        "2, on-created-empty:emacs"
        "3, on-created-empty:firefox"
      ]);
    };
  };

  services = {
    dunst.enable = true;
    gnome-keyring.enable = true;
  };
}
