{config, desktop, laptop, lib, nix-colors, nixvim, ...}:
{
  imports = [
	nix-colors.homeManagerModules.default
    nixvim.homeManagerModules.nixvim
    ./gui.nix
    ./hyprland.nix
    ./editors/main.nix
    ./cli/main.nix
    ./themes/main.nix
  ];


  options = {
    laptop.enable = lib.mkEnableOption "Enable laptop configuration";
    desktop.enable = lib.mkEnableOption "Enable laptop configuration";
  };

  config = {
    themes.gruvbox.enable = true;
    laptop.enable = laptop;
    desktop.enable = desktop;
    home = {
      homeDirectory = "/home/kacper";
      shellAliases = {
        week = "date +%V";
        xmrig = "sudo xmrig -c $HOME/.config/xmrig.json";
      };
      username = "kacper";
      stateVersion = if config.desktop.enable then "23.11" else "23.05";
    };
  };

}
