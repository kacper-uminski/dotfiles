{...}:
{
  programs.eza = {
    enable = true;
    git = true;
    icons = true;
  };
  home.shellAliases.tree = "eza --tree";
}
