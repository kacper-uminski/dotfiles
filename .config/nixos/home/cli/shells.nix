{...}:
{
  programs = {
    bash = {
      enable = true;
      enableCompletion = true;
      historyFile = ".config/bash/bash_history";
    };

    zsh = {
      enable = true;
      autosuggestion.enable = true;
      enableCompletion = true;
      syntaxHighlighting.enable = true;
      dotDir = ".config/zsh";
      history.path = "$ZDOTDIR/zsh_history";
      loginExtra = "if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then Hyprland; fi";
    };
  };
}
