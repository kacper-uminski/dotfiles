{
  enable = true;
  matchBlocks = {

    liu = {
      forwardX11 = true;
      forwardX11Trusted = true;
      hostname = "ssh.edu.liu.se";
      setEnv = {
        TERM = "xterm-256color";
      };
      user = "kacum383";
    };

    "muxen?-???" = {
      forwardX11 = true;
      forwardX11Trusted = true;
      hostname = "%h.ad.liu.se";
      proxyJump = "liu";
      setEnv = {
        TERM = "xterm-256color";
      };
      user = "kacum383";
    };

    "shipon totoro viridian" = {
      forwardX11 = true;
      forwardX11Trusted = true;
      hostname = "%h.lysator.liu.se";
      setEnv = {
        TERM = "xterm-256color";
      };
      user = "kacper";
    };

    "ssh.edu.liu.se" = {
      match = ''host ssh.edu.liu.se !exec "nc -zw1 %h 22"'';
      proxyJump = "viridian";
    };
  };
}
