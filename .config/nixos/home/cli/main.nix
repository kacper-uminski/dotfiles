{lib, ...}:
{
  imports = [
    ./bat.nix
    ./eza.nix
    ./git.nix
    ./shells.nix
  ];

  programs = {
    
    htop.enable = true;

    ripgrep.enable = true;

    ssh = import ./ssh.nix;

    starship = {
      enable = true;
      settings = {
        add_newline = false;
        format = lib.concatStrings [
          "$all"
          "$directory"
          "$character"
        ];
      };
    };

    zoxide = {
      enable = true;
      options = [
        "--cmd cd"
      ];
    };
  };
}
