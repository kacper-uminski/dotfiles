{...}:
{
  programs = {
    git = {
      enable = true;
      userName = "kacper-uminski";
      userEmail = "kacperuminski@protonmail.com";
    };
    gitui.enable = true;
    lazygit.enable = true;
  };
  home.shellAliases.dotfiles = "git --git-dir=$HOME/Software/dotfiles --work-tree=$HOME"; 
}
